from django.apps import AppConfig


class FaskesConfig(AppConfig):
    name = 'faskes'
