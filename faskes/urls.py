from django.urls import path
from django.contrib.auth import views as av
from .views import create_faskes, read_faskes, detail_faskes

app_name = 'faskes'
urlpatterns = [
    path('faskes/create_faskes', create_faskes, name='create_form_faskes'),
    path('faskes/daftar_faskes', read_faskes, name='daftar_faskes'),
    path('faskes/detail_faskes/<str:row>', detail_faskes, name='detail_faskes'),
]