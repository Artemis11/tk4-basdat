from django.shortcuts import render
from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateFaskes

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    columns = [col[0] for col in desc]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]

def create_faskes(request):
    code = ''
    with connection.cursor() as c:
        c.execute("select count(*) from siruco.faskes")
        num = c.fetchone()
        c.close()
        num_new = num[0]+1
        num_str = str(num_new)
        if num_new < 10:
            code = 'F0'+ num_str
        else :
            code = 'F' + num_str

    initialdict = {"kode" : code}

    response = {}
    response['error'] = False
    form = CreateFaskes(request.POST or None, initial = initialdict) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            kode_faskes = form.cleaned_data['kode']
            tipe = request.POST.get('tipe')
            nama_faskes = request.POST.get('nama_faskes')
            status_kepemilikan = request.POST.get('status_kepemilikan')
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabkot = request.POST.get('kabkot')
            provinsi = request.POST.get('provinsi')

            with connection.cursor() as c:
                c.execute("set search_path to siruco")
                c.execute("insert into faskes values ('{}','{}','{}','{}','{}','{}','{}','{}','{}');".format(kode_faskes,tipe,nama_faskes,status_kepemilikan,jalan,kelurahan,kecamatan,kabkot,provinsi))
                return redirect('faskes:list_faskes') 

    form = CreateFaskes()

    arguments = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'createfaskes.html', arguments)

    
def read_faskes(request):
    c = connection.cursor()
    c.execute('set search_path to siruco')
    c.execute('select kode, nama, tipe from faskes')
    list_faskes = namedtuplefetchall(c)
    return render(request, 'daftarfaskes.html', {'list_faskes': list_faskes})

def detail_faskes(request, row):
    c = connection.cursor()
    c.execute('set search_path to siruco')
    c.execute('select * from faskes where kode = %s;',[row])
    datafaskes = namedtuplefetchall(c)
    response = {}
    response['kode'] = datafaskes[0]
    response['tipe'] = datafaskes[1]
    response['nama'] = datafaskes[2]
    response['status_kepemilikan'] = datafaskes[3]
    response['jalan'] = datafaskes[4]
    response['kelurahan'] = datafaskes[5]
    response['kecamatan'] = datafaskes[6]
    response['kabkot'] = datafaskes[7]
    response['provinsi'] = datafaskes[8]
    c.close()
    return render(request, 'detailfaskes.html', response)