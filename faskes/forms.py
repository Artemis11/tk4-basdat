from django import forms
from django.db import connection

tipeChoices = [('Rumah Sakit','Rumah Sakit'),('Puskesmas','Puskesmas'),('Klinik','Klinik')]

ownerChoices = [('Swasta','Swasta'),('Pemerintah','Pemerintah')]

class CreateFaskes(forms.Form):
    kode =  forms.CharField(label='Kode Faskes', disabled=True, required=True)

    tipe = forms.ChoiceField(label="Tipe", widget = forms.Select(choices = tipeChoices))
    
    nama_faskes = forms.CharField(label='Nama Faskes', widget=forms.TextInput(attrs={
        'placeholder': 'Nama Faskes',
    }))
    
    status_kepemilikan = forms.ChoiceField(label="Status Kepemilikan", widget = forms.Select(choices = ownerChoices)), 
    jalan = forms.CharField(label='Jalan', widget=forms.TextInput(attrs={
        'placeholder': 'Alamat',
    }))

    kelurahan = forms.CharField(label='Kelurahan', widget=forms.TextInput(attrs={
        'placeholder': 'Kelurahan',
    }))

    kecamatan = forms.CharField(label='Kecamatan', widget=forms.TextInput(attrs={
        'placeholder': 'Kecamatan',
    }))

    kabkot = forms.CharField(label='Kabupaten/Kota', widget=forms.TextInput(attrs={
        'placeholder': 'Kabupaten/Kota',
    }))

    provinsi = forms.CharField(label='Provinsi', widget=forms.TextInput(attrs={
        'placeholder': 'Provinsi',
    }))
