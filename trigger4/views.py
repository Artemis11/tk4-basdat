from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import *
from datetime import date
from users.decorators import unauthorized_user_redirect_home, authenticated_user_required
from django.db import connection, transaction, IntegrityError

today = date.today()