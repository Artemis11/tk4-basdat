from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.db.models.fields import BLANK_CHOICE_DASH

daftar_kode_hotel = ['A2001', 'A2002', 'A22003', 'A2004', 'A2005']
daftar_nik =[]

class RuanganHotelForm(forms.Form):
    kode_hotel = forms.CharField(widget=forms.Select(choices=[(x,x) for x in daftar_kode_hotel]))
    kode_ruangan = forms.CharField(max_length=6, required = "True")
    jenis_bed = forms.CharField(max_length=20, required = "True")
    tipe = forms.CharField(max_length=20, required = "True")
    harga = forms.IntegerField(initial=0)

class ReservasiHotelForm(forms.Form):
    nik = forms.CharField(widget=forms.Select(choices=[(x,x) for x in daftar_nik]))
    tgl_masuk = forms.CharField(max_length=10, required = "True")
    tgl_keluar = forms.CharField(max_length=10, required = "True")
    kode_hotel = forms.CharField(widget=forms.Select(choices=[(x,x) for x in daftar_kode_hotel]))
    # kode_ruangan = forms.CharField(widget=forms.Select(choices=[(x,x) for x in ]))
    status = forms.CharField(max_length=10, required='True')

# class TransaksiHotelForm(forms.Form):


