from django.shortcuts import render
from django.db import connection
from .forms import CreateJadwal

def create_jadwal(request):
    response = {}
    response['error'] = False
    form = CreateJadwal(request.POST or None) 
    response['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            kode = request.POST.get('kode')
            shift = request.POST.get('shift')
            tanggal = request.POST.get('tanggal')

            with connection.cursor() as cursor:
                cursor.execute("set search_path to siruco;")
                cursor.execute("insert into jadwal values ('{}','{}','{}');".format(kode,shift,tanggal))
                return redirect('faskes:list_jadwal') 

    form = CreateJadwal()

    args = {
        'form' : response['form'],
        'username' : username,
        'password' : password,
        'peran' : peran
    }
    return render(request, 'create_jadwal.html', args)

def read_jadwal(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to siruco;')
    cursor.execute('select kode_faskes, shift, tanggal from jadwal;')
    list_jadwal = cursor.fetchall()
    return render(request, 'list_jadwal.html', {'list_jadwal': list_jadwal})
