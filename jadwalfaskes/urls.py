from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'jadwalfaskes'

urlpatterns = [
    path('jadwalfaskes/create_jadwal', views.create_jadwal, name='create_jadwal'),
    path('jadwalfaskes/list_jadwal', views.read_jadwal, name='read_jadwal')
]
