from django.apps import AppConfig


class JadwalfaskesConfig(AppConfig):
    name = 'jadwalfaskes'
