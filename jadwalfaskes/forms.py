from django import forms
from django.db import connection

class CreateJadwal(forms.Form):   
    def get_all_kode_faskes():
        c = connection.cursor()
        c.execute("set search_path to siruco;")
        c.execute("select kode, nama from faskes;")
        hasil = c.fetchall()
        c.close()
        return hasil

    kode = forms.ChoiceField(label="Kode Faskes", choices= get_all_kode_faskes(), required=True)

    shift = forms.CharField(label='Shift Faskes', widget=forms.TextInput(attrs={
        'placeholder': 'Shift Faskes',
    }))
   
    tanggal = forms.DateField(label ='Tanggal',required=True)
