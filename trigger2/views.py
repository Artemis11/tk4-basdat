from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.http import JsonResponse
import datetime
from django.contrib import messages
import random
import string

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read_ruangan(request):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM SIRUCO.RUANGAN_RS")
    hasil = namedtuplefetchall(cursor)
    context = {
        'hasil':hasil
    }
    cursor.close()
    return render (request, "list_ruangan.html",context)

def read_bed(request):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM SIRUCO.BED_RS")
    hasil = namedtuplefetchall(cursor)
    context = {
        'hasil':hasil
    }
    cursor.close()
    return render (request, "list_bed.html",context)

def read_jadwal_dokter(request):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM SIRUCO.JADWAL_DOKTER")
    hasil = namedtuplefetchall(cursor)
    context = {
        'hasil':hasil
    }
    cursor.close()
    return render (request, "list_jadwal_dokter.html",context)
