from django.urls import path
from . import views

app_name = 'trigger2'

urlpatterns = [
    path('read_ruangan/', views.read_ruangan, name='read_ruangan'),
    path('read_bed/', views.read_bed, name='read_bed'),
    path('read_jadwal_dokter/', views.read_jadwal_dokter, name='read_jadwal_dokter')
]
