from django import forms
from django.db import connection

class CreateReservasiRS (forms.Form):
    def get_all_pasien():
        c = connection.cursor()
        c.execute("set search_path to siruco;")
        c.execute("select nik from pasien;")
        result = c.fetchall()
        c.close()
        return result

    def get_all_koders():
        c = connection.cursor()
        c.execute("set search_path to siruco;")
        c.execute("select kode_faskes from rumah_sakit;")
        result = c.fetchall()
        c.close()
        return result

    nik = forms.ChoiceField(label = "NIK", choices = get_all_pasien())

    tglmasuk = forms.CharField(label='Tanggal Masuk', widget=forms.DateInput(attrs={
        'placeholder': 'Tanggal Masuk',
        }))

    tglkeluar = forms.CharField(label='Tanggal Keluar', widget=forms.DateInput(attrs={
        'placeholder': 'Tanggal Keluar',
        }))
    
    kode_rs = forms.ChoiceField(label="Kode RS", choices= get_all_koders(), required=True)

    kode_ruangan = forms.CharField(label="Kode Ruangan", required = True, widget=forms.Select(choices=[]))

    kode_bed = forms.CharField(label="Kode Bed", required=True, widget=forms.Select(choices=[]))

