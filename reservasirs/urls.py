from django.urls import path
from django.contrib.auth import views as av
from . import views

app_name = 'reservasirs'

urlpatterns = [
    path('reservasirs/create_reservasi', views.create_reservasi_rs, name='create_form_reservasi_rs'),
    path('reservasirs/list_ruangan', views.get_ruangan, name='list_ruang'),
    path('reservasirs/list_bed', views.get_bed, name='list_bed'),
    path('reservasirs/daftar_reservasi', views.read_reservasi_rs, name='list_reservasi'),
]
