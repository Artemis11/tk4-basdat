from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateReservasiRS
# Create your views here.

def create_reservasi_rs(request):
    response = {}
    response["error"] = False
    form = CreateReservasiRS(request.POST or None) 
    response["form"] = form
    if request.method == "POST":
        if form.is_valid():
            NIK_Pasien = form.cleaned_data['nik']
            tanggal_masuk = request.POST.get('tglmasuk')
            tanggal_keluar = request.POST.get('tglkeluar')
            kode_rs = form.cleaned_data['kode_rs']
            kode_ruangan = form.cleaned_data['kode_ruangan']
            kode_bed = form.cleaned_data['kode_bed']
            
            with connection.cursor() as cursor:
                cursor.execute("insert into reservasi_rs values ('{}','{}','{}','{}','{}','{}');".format(NIK_Pasien,tanggal_masuk, tanggal_keluar, kode_rs, kode_ruangan, kode_bed ))
                return redirect("rumahsakit:list_reservasi")

    form = CreateReservasiRS()
    arguments = {
        'form' : response['form'],
    }
    return render(request, 'createreservasirs.html', arguments)

def get_ruangan(request):
    rumah_sakit = request.GET.get('kode_rs')
    list_ruangan = []

    with connection.cursor() as cursor:
        cursor.execute("select koderuangan from ruangan_rs where koders = '{}' and jmlbed > 0;".format(rumah_sakit))
        list_ruangan = cursor.fetchall()

    daftar_akhir = []
    for i in list_ruangan:
        val = (i[0],i[0])
        daftar_akhir.append(val)
    return render(request, 'listruangan.html', {'listruangan':daftar_akhir})

def get_bed(request):
    rumah_sakit = request.GET.get('kode_rs')
    ruangan = request.GET.get('kode_ruang')
    list_bed = []

    with connection.cursor() as cursor:
        cursor.execute("select kodebed from bed_rs where koders = '{}' and koderuangan = '{}';".format(rumah_sakit,ruangan))
        list_bed = cursor.fetchall()

    final = [] 
    for i in list_bed:
        val = (i[0],i[0])
        final.append(val)
    return render(request, 'listbed.html', {'listbed':final})

def read_reservasi_rs(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to siruco;")
    cursor.execute("select kodepasien, tglmasuk, tglkeluar, koders, koderuangan, kodebed from reservasi_rs;" )
    daftarreservasi = cursor.fetchall()
    arguments = {"daftarreservasi":daftarreservasi}
    return render(request, "daftarreservasi.html", arguments)
