from django.shortcuts import render, redirect
from .forms import *
from django.db import IntegrityError, connection
from collections import namedtuple
from random import randrange
from django.http import request

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request, validasi = None):
    try:
        username = request.session['email']
        password = request.session['password']
        return login(request)
    except KeyError:
        formulir = LoginForm()
        message = ""
        if(validasi==False):
            message = "Invalid email or password"
        argument = { 
            'formlogin' : formulir,
            'message' : message
        }
        return render(request, 'login.html', argument)
    
def login(request):
    try:
        username = request.session['email']
        password = request.session['password']
    except:
        username = request.POST['email']
        password = request.POST['password']

    username = str(username)
    password = str(password)
    peran = ""
    with connection.cursor() as c:
        c.execute("set search_path to public")
        c.execute("select * from siruco.akun_pengguna where username='"+username+"' and password='"+password+"'")
        hasil = namedtuplefetchall(c)
        print(hasil)
        c.execute("select usernameadm from siruco.admin where usernameadm='"+username+"'")
        cekadmin = namedtuplefetchall(c)
        print(cekadmin)
        c.execute("select username from siruco.dokter where username='"+username+"'")
        cekdokter = namedtuplefetchall(c)
        print(cekdokter)
        c.execute("select usernamesatgas from siruco.admin_satgas where usernamesatgas='"+username+"'")
        ceksatgas = namedtuplefetchall(c) 
        print(ceksatgas)
        if(cekadmin != []):
            if (cekadmin == cekdokter):
                peran = 'Dokter'
            if (cekadmin == ceksatgas):
                peran = 'Admin Satgas'
            else:
                peran = 'Admin Sistem'
        else:
            peran = 'Pengguna Publik'

        c.execute("set search_path to public")
        if (hasil == []):
            c.close()
            return index(request, False)
        else:
            request.session['username'] = hasil[0].username
            request.session['password'] = hasil[0].password
            request.session['peran'] = hasil[0].peran
            c.close()
            return render(request, "home.html")

def logout(request):
    request.session.flush()
    request.session.clear_expired()
    return index(request)


# def registrasi(request):

# # Masih harus diedit
# def create_pasien(request):
#     # c.execute("set search_path to public")
#     # if request.session["peran"] == "rolePublik":
#     context = {}
#     if request.method == 'POST':
#         with connection.cursor() as c:
#             nik_pasien = request.POST["nik_pasien"]
#             id_pendaftar = request.POST["id_pendaftar"]
#             nama_pasien = request.POST["nama_pasien"]
#             ktp_jalan = request.POST["ktp_jalan"]
#             ktp_kelurahan = request.POST["ktp_kelurahan"]
#             ktp_kecamatan = request.POST["ktp_kecamatan"]
#             ktp_kabkot = request.POST["ktp_kabkot"]
#             ktp_prov = request.POST["ktp_prov"]
#             dom_jalan = request.POST["dom_jalan"]
#             dom_kelurahan = request.POST["dom_kelurahan"]
#             dom_kecamatan = request.POST["dom_kecamatan"]
#             dom_kabkot = request.POST["dom_kabkot"]
#             dom_prov = request.POST["dom_prov"]
#             no_telp = request.POST["no_telp"]
#             no_hp = request.POST["no_hp"]
#             c.execute(f"insert into pasien values ('{nik_pasien}', '{id_pendaftar}','{nama_pasien}','{ktp_jalan}','{ktp_kelurahan}','{ktp_kecamatan}','{ktp_kabkot}','{ktp_prov}','{dom_jalan}','{dom_kelurahan}','{dom_kecamatan}','{dom_kabkot}','{dom_prov}','{no_telp}','{no_hp}')")
#             return redirect('read_pasien')
#     return render(request, 'create_pasien.html')

#Update Pasien 
def update_pasien(request, nik, message=None): 
    nik = str(nik)
    cursor = connection.cursor()
    sql = "select * from pasien where nik = %s limit 1"
    cursor.execute(sql, [nik])
    pasien = namedtuplefetchall(cursor)
    pendaftar = pasien[0].idpendaftar
    nik = pasien[0].nik
    nama = pasien[0].nama
    no_telp = pasien[0].notelp
    no_hp = pasien[0].nohp
    jalan_ktp = pasien[0].ktp_jalan
    kelurahan_ktp = pasien[0].ktp_kelurahan
    kecamatan_ktp = pasien[0].ktp_kecamatan
    kabkot_ktp = pasien[0].ktp_kabkot
    provinsi_ktp = pasien[0].ktp_prov
    jalan_dom = pasien[0].dom_jalan
    kelurahan_dom = pasien[0].dom_kelurahan
    kecamatan_dom = pasien[0].dom_kecamatan
    kabkot_dom = pasien[0].dom_kabkot
    provinsi_dom = pasien[0].dom_prov

    form_update_pasien = update_pasien_form(initial={
        'pendaftar':pendaftar,
        'nik':nik,
        'nama':nama,
        'no_telp':no_telp,
        'no_hp':no_hp,
        'jalan_ktp':jalan_ktp,
        'kelurahan_ktp':kelurahan_ktp,
        'kecamatan_ktp':kecamatan_ktp,
        'kabkot_ktp':kabkot_ktp,
        'provinsi_ktp':provinsi_ktp,
        'jalan_dom':jalan_dom,
        'kelurahan_dom':kelurahan_dom,
        'kecamatan_dom':kecamatan_dom,
        'kabkot_dom':kabkot_dom,
        'provinsi_dom':provinsi_dom,
    })

    if request.method == "POST":
        # if form_update_pasien.is_valid():
        pendaftar = request.POST.get("idpendaftar", None)
        nik = request.POST.get("nik")
        no_telp = request.POST.get("notelp")
        no_hp = request.POST.get("nohp")

        jalan_ktp = request.POST.get("ktp_jalan")
        kelurahan_ktp = request.POST.get("ktp_kelurahan")
        kecamatan_ktp = request.POST.get("ktp_kecamatan")
        kabkot_ktp = request.POST.get("ktp_kabkot")
        provinsi_ktp = request.POST.get("ktp_prov")

        jalan_dom = request.POST.get("dom_jalan")
        kelurahan_dom = request.POST.get("dom_kelurahan")
        kecamatan_dom = request.POST.get("dom_kecamatan")
        kabkot_dom = request.POST.get("dom_kabkot")
        provinsi_dom = request.POST.get("dom_provinsi")
        pasien_update = [no_telp, no_hp, jalan_ktp, kelurahan_ktp, kecamatan_ktp, kabkot_ktp, provinsi_ktp, jalan_dom, kelurahan_dom, kecamatan_dom, kabkot_dom, provinsi_dom, nik]

        if(no_telp=='' or no_hp=='' or jalan_ktp=='' or kelurahan_ktp=='' or kecamatan_ktp=='' or kabkot_ktp=='' or provinsi_ktp=='' or jalan_dom=='' or kelurahan_dom=='' or kecamatan_dom=='' or kabkot_dom=='' or provinsi_dom==''):
            message = 'Data pasien belum lengkap. Pastikan semua field terisi.'
            return update_pasien(request, nik, message)
        sql = "update pasien set notelp=%s, nohp=%s, ktp_jalan=%s, ktp_kelurahan=%s, ktp_kecamatan=%s, ktp_kabkot=%s, ktp_prov=%s, dom_jalan=%s, dom_kelurahan=%s,dom_kecamatan=%s, dom_kabkot=%s, dom_prov=%s WHERE nik=%s"
        cursor.execute(sql, pasien_update)
        return redirect('read_pasien')

    # message = ''
    context = {
        'form_update_pasien':form_update_pasien,
        'message':message,
        'nik':nik
    }
    return render(request, "update_pasien.html", context)

#Delete Pasien
def delete_pasien(request, nik):
    nik = str(nik)
    cursor = connection.cursor()
    sql = "delete from pasien where nik = %s"
    cursor.execute(sql, [nik])
    cursor.close()
    return redirect('read_pasien')

# # Masih harus diedit
# def read_pasien(request):
#     if request.session["peran"] == "rolePublik":
#         context = {}
#         with connection.cursor() as c:
#             c.execute("select * from pasien")
#             context["daftar_pasien"] = c.fetchall()
#         return render(request, 'read_pasien.html', context)


