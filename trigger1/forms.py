from django import forms

daftar_kode_faskes = ['D60', 'B48', 'T72', 'B90', 'D93', 'B15', 'D21', 'B84', 'D55','B10']

class LoginForm(forms.Form):
    email = forms.CharField(label = 'Email', widget=forms.TextInput(
        attrs={'placeholder': 'Email'}
    ))

    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput(
        attrs={'placeholder': 'Password'}
    ))

class create_pasien(forms.Form):
    nik = forms.CharField(label = 'nik_pasien', widget=forms.IntegerField(
        attrs={'placeholder': 'NIK Pasien'}
    ))

    id_pendaftar = forms.CharField(label = 'id_pendaftar',  widget=forms.EmailInput(
        attrs={'placeholder': 'ID Pendaftar'}
    ))

    nama_pasien = forms.CharField(label = 'nama_pasien', widget=forms.TextInput(
        attrs={'placeholder': 'Nama Pasien'}
    ))

    ktp_jalan = forms.CharField(label = 'ktp_jalan',  widget=forms.TextInput(
        attrs={'placeholder': 'Jalan (sesuai KTP)'}
    ))

    ktp_kelurahan = forms.CharField(label = 'ktp_kelurahan',  widget=forms.TextInput(
        attrs={'placeholder': 'Kelurahan (sesuai KTP)'}
    ))

    ktp_kecamatan = forms.CharField(label = 'ktp_lecamatan',  widget=forms.TextInput(
        attrs={'placeholder': 'Kecamatan (sesuai KTP)'}
    ))

    ktp_kabkot = forms.CharField(label = 'ktp_kabkot',  widget=forms.TextInput(
        attrs={'placeholder': 'Kabupaten/kota (sesuai KTP)'}
    ))

    ktp_prov = forms.CharField(label = 'ktp_prov',  widget=forms.TextInput(
        attrs={'placeholder': 'Provinsi (sesuai KTP)'}
    ))

    dom_jalan = forms.CharField(label = 'dom_jalan',  widget=forms.TextInput(
        attrs={'placeholder': 'Jalan (sesuai domisili)'}
    ))

    dom_kelurahan = forms.CharField(label = 'dom_kelurahan',  widget=forms.TextInput(
        attrs={'placeholder': 'Kelurahan (sesuai domisili)'}
    ))

    dom_kecamatan = forms.CharField(label = 'dom_lecamatan',  widget=forms.TextInput(
        attrs={'placeholder': 'Kecamatan (sesuai domisili)'}
    ))

    dom_kabkot = forms.CharField(label = 'dom_kabkot',  widget=forms.TextInput(
        attrs={'placeholder': 'Kabupaten/kota (sesuai domisili)'}
    ))

    dom_prov = forms.CharField(label = 'dom_prov',  widget=forms.TextInput(
        attrs={'placeholder': 'Provinsi (sesuai domisili)'}
    ))

    no_telp = forms.CharField(label = 'no_telp', widget=forms.IntegerField(
        attrs={'placeholder': 'Nomor Telpon Pasien'}
    ))

    no_hp = forms.CharField(label = 'no_hp', widget=forms.IntegerField(
        attrs={'placeholder': 'Nomor HP Pasien'}
    ))


class update_pasien_form(forms.Form):
    pendaftar = forms.CharField(label='Pendaftar', disabled=True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    nik = forms.CharField(label='NIK', disabled=True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    nama = forms.CharField(label='Nama', disabled=True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    no_telp = forms.CharField(label='Nomor Telepon', max_length=20, required=True)
    no_hp = forms.CharField(label='Nomor HP', max_length=12, required=True)

    jalan_ktp = forms.CharField(label='Jalan', max_length=30, required=True)
    kelurahan_ktp = forms.CharField(label='Kelurahan', max_length=30, required=True)
    kecamatan_ktp = forms.CharField(label='Kecamatan', max_length=30, required=True)
    kabkot_ktp = forms.CharField(label='Kabupaten/Kota', max_length=30, required=True)
    provinsi_ktp = forms.CharField(label='Provinsi', max_length=30, required=True)

    jalan_dom = forms.CharField(label='Jalan', max_length=30, required=True)
    kelurahan_dom = forms.CharField(label='Kelurahan', max_length=30, required=True)
    kecamatan_dom = forms.CharField(label='Kecamatan', max_length=30, required=True)
    kabkot_dom = forms.CharField(label='Kabupaten/Kota', max_length=30, required=True)
    provinsi_dom = forms.CharField(label='Provinsi', max_length=30, required=True)

class FormAdminSistem(forms.Form):
    email = forms.CharField(label = 'Email' , max_length=50, required = "True")
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128, required = "True")

class FormPenggunaPublik(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, required = "True")
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128, required = "True")
    nama = forms.CharField(label = 'Nama', max_length=50, required = "True")
    nik = forms.CharField(label = 'NIK', max_length=20, required = "True")
    no_telp = forms.CharField(label = 'No. HP',max_length=20, required = "True")

class FormDokter(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, required = "True")
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128, required = "True")
    no_str = forms.CharField(label = 'No. STR', max_length = 20, required = "True")
    nama = forms.CharField(label = 'Nama', max_length=50, required = "True")
    no_telp = forms.CharField(label = 'No. HP', max_length=20, required = "True")
    gelar_depan = forms.CharField(label = 'Gelar Depan', max_length=10, required = "True")
    gelar_belakang = forms.CharField(label = 'Gelar Belakang', max_length=10, required = "True")

class FormAdminSatgas(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50, required = "True")
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128, required = "True")
    kode_faskes = forms.CharField(widget=forms.Select(choices=[(x,x) for x in daftar_kode_faskes]))

# class create_pasien(forms.Form):
#     nik = forms.CharField(label = 'nik_pasien', widget=forms.IntegerField(
#         attrs={'placeholder': 'NIK Pasien'}
#     ))

#     id_pendaftar = forms.CharField(label = 'id_pendaftar',  widget=forms.EmailInput(
#         attrs={'placeholder': 'ID Pendaftar'}
#     ))

#     nama_pasien = forms.CharField(label = 'nama_pasien', widget=forms.TextInput(
#         attrs={'placeholder': 'Nama Pasien'}
#     ))

#     ktp_jalan = forms.CharField(label = 'ktp_jalan',  widget=forms.TextInput(
#         attrs={'placeholder': 'Jalan (sesuai KTP)'}
#     ))

#     ktp_kelurahan = forms.CharField(label = 'ktp_kelurahan',  widget=forms.TextInput(
#         attrs={'placeholder': 'Kelurahan (sesuai KTP)'}
#     ))

#     ktp_kecamatan = forms.CharField(label = 'ktp_lecamatan',  widget=forms.TextInput(
#         attrs={'placeholder': 'Kecamatan (sesuai KTP)'}
#     ))

#     ktp_kabkot = forms.CharField(label = 'ktp_kabkot',  widget=forms.TextInput(
#         attrs={'placeholder': 'Kabupaten/kota (sesuai KTP)'}
#     ))

#     ktp_prov = forms.CharField(label = 'ktp_prov',  widget=forms.TextInput(
#         attrs={'placeholder': 'Provinsi (sesuai KTP)'}
#     ))

#     dom_jalan = forms.CharField(label = 'dom_jalan',  widget=forms.TextInput(
#         attrs={'placeholder': 'Jalan (sesuai domisili)'}
#     ))

#     dom_kelurahan = forms.CharField(label = 'dom_kelurahan',  widget=forms.TextInput(
#         attrs={'placeholder': 'Kelurahan (sesuai domisili)'}
#     ))

#     dom_kecamatan = forms.CharField(label = 'dom_lecamatan',  widget=forms.TextInput(
#         attrs={'placeholder': 'Kecamatan (sesuai domisili)'}
#     ))

#     dom_kabkot = forms.CharField(label = 'dom_kabkot',  widget=forms.TextInput(
#         attrs={'placeholder': 'Kabupaten/kota (sesuai domisili)'}
#     ))

#     dom_prov = forms.CharField(label = 'dom_prov',  widget=forms.TextInput(
#         attrs={'placeholder': 'Provinsi (sesuai domisili)'}
#     ))

#     no_telp = forms.CharField(label = 'no_telp', widget=forms.IntegerField(
#         attrs={'placeholder': 'Nomor Telpon Pasien'}
#     ))

#     no_hp = forms.CharField(label = 'no_hp', widget=forms.IntegerField(
#         attrs={'placeholder': 'Nomor HP Pasien'}
#     ))

